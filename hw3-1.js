var MongoClient = require('mongodb').MongoClient

MongoClient.connect("mongodb://localhost:27017/school", {native_parser:true}, function(err, db) {
    
    
    db.collection('students').find().toArray(function(err,docs) {
        
        for(var i = 0; i < docs.length; i++) {
            var scores = docs[i].scores;
            var name = docs[i].name;
            var id = docs[i]._id;
            var hw_scores = new Array();
            for(var j = 0; j < scores.length; j++) {
                var type = scores[j]['type'];
                var score = scores[j]['score'];
                if(type == 'homework') {
                    hw_scores.push(score);
                }
            }
            console.log(name + " : " + hw_scores.sort());
            console.log(hw_scores.length);
            if(hw_scores.length > 1) {
                 db.collection('students').update({_id: id}, { '$pull': { "scores" : { 'type': 'homework', 'score' : hw_scores[0] } } }, function(err, doc) {
                    if(err) throw err;
                    console.log(doc);
                    //return db.close();
                });               
            }

        }
        //return db.close();
    }); // end db.collection.find()
        
        
});
