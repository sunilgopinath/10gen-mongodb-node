10gen's course in node.js
=============

Getting started
-------------
*   Checkout the repo:

        bash> git clone https://sunilgopinath@bitbucket.org/sunilgopinath/10gen-mongodb-node.git
        
*   Install the dependencies:

        bash> npm install
        
*   Run the app:

        bash> node app.js		

Loading some data
------------------
we are using the `m101` database and the `wk1` collection so we need to insert some data to get a return value from running the app.

    db.wk1.insert({'name' : 'MongoDB'})

### For homework 2.2

To import data from 10gen's mongodb 101N course into the local mongo instance we need to do

    mongoimport --type csv --headerline ~/data/weather_data.7140f46b3900.csv -d weather -c data

