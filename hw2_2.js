var MongoClient = require('mongodb').MongoClient

MongoClient.connect("mongodb://localhost:27017/weather", {native_parser:true}, function(err, db) {
    
    var query = {}
    var projection = {'State' : 1, 'Temperature' : 1}
    var options = { 'sort' : [['State', 1], ['Temperature', -1]]};
    
    db.collection('data').find({},projection, options).toArray(function(err,docs) {
        if(err) throw err;
        
        var currState = "";
        var highest_temps = [];
        for(var i = 0; i < docs.length; i++) {
            if(currState != docs[i].State) {
                currState = docs[i].State;
                highest_temps.push(docs[i]._id);
                console.log(docs[i].State + " - " + docs[i].Temperature);
            }
        }

        operator = {'$set' : {'month_high' : true}}
        for(var i = 0; i < highest_temps.length;i++) {
            db.collection('data').update({'_id' : highest_temps[i]}, operator, function(err,updated) {
                if(err) throw err;
                
                console.log("doc successfully updated " + updated);
            });
            console.log(highest_temps[i]);
        }
        //db.close();
    }); // end db.collection.find()
        
        
});
